# Code for Parton Shower Uncertainty Checks

## **Descriptions**

- The code is inspired by [Zirui's code](https://gitlab.cern.ch/ziwang/truth_derivation/-/tree/master) for $H\gamma\gamma$ parton shower uncertainty checks
- The code performs basic kinematic cuts on DAOD files to check acceptance, the variation of acceptance will be used to estimate the systematics

## **How to Setup**

### First Setup

```shell
git clone ssh://git@gitlab.cern.ch:7999/low-mass-zprime-checks/check_ps_sys.git
cd check_ps_sys
mkdir run build
cd build
acmSetup --sourcedir=../source AnalysisBase,21.2.68
acm compile
cd ..
cd run
```

### Recurrent Setup

```shell
source setup.sh
```

## **How to Run**

in "run" folder

```shell
PS_truth.py [Path to DAOD folder] [Output folder]
```

example:

```shell
PS_truth.py /net/s3_datae/yangz/zprime/DAOD/mc15_13TeV.345706.Sherpa_222_NNPDF30NNLO_ggllll_130M4l.deriv.DAOD_TRUTH3.e6213_e5984_p3655/ test_run
```
