#ifndef jxAOD_jxAODAnalysisBase_H
#define jxAOD_jxAODAnalysisBase_H

#include <AnaAlgorithm/AnaAlgorithm.h>

#include "jxAOD/jCutFlow.h"
#include "jxAOD/Counting.h"
#include "AsgTools/AnaToolHandle.h"
//#include "PMGAnalysisInterfaces/IPMGTruthWeightTool.h"
#include "PMGTools/PMGTruthWeightTool.h"

#include <TH1.h>


class jxAODAnalysisBase : public EL::AnaAlgorithm
{
    public:
        // this is a standard algorithm constructor
        jxAODAnalysisBase (const std::string& name, ISvcLocator* pSvcLocator);

        // these are the functions inherited from Algorithm
        virtual StatusCode initialize () override;
        virtual StatusCode execute () override;
        virtual StatusCode finalize () override;
        virtual StatusCode fileExecute() override;

    private:
        xAOD::TEvent *m_event;

        jCutFlow *cutflow;

        asg::AnaToolHandle<PMGTools::IPMGTruthWeightTool> m_weightTool;

        uint64_t nAcceptedEvents;
        double   sumOfEventWeights;
        double   sumOfEventWeightsSquared;
        double   weight_pdf;
        double   weight;
        std::vector<double> vweight;
        std::vector<std::string> iKey;
        std::vector<std::string> dKey;
        std::map<std::string, int> iMap;
        std::map<std::string, double> dMap;

        // Configuration, and any other types of variables go here.
        //float m_cutValue;
        //TTree *m_myTree;
        //TH1 *m_myHist;
};

#endif
