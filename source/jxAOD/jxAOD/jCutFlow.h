#ifndef JXAOD_JCUTFLOW_H
#define JXAOD_JCUTFLOW_H

#include <AsgTools/MessageCheck.h>
#include "EventLoop/StatusCode.h"

#include "TH1I.h"
#include "TString.h"

#include <functional>
#include <vector>

#include "jxAOD/jTree.h"

using namespace asg::msgUserCode;

typedef std::function<bool(int)> jFunc;
//typedef std::vector<jFunc> jFuncVec;

class jCut
{
    public:
        jCut (jFunc func, TString cutname, int index);
        jCut (jFunc func);

        TString Name(){return this->cutname_;}
        jFunc Func(){return this->func_;}
        int Index(){return this->index_;}
        bool Pass(){return this->pass_;}
        float Weight(){return this->weight_;}

        bool SetPass(bool pass){this->pass_ = pass; return true;}
        bool SetWeight(float weight){this->weight_ = weight; return true;}

    private:
        TString cutname_;
        jFunc func_;
        int index_;
        bool pass_;
        float weight_;
};

typedef std::vector<jCut*> jCutVec;

class jCutFlow 
{
    public:
        jCutFlow (TString cutflowname);

        TString Name(){return this->cutflowname_;}
        int Size(){return this->numcuts_;}

        bool AddCut(jFunc func, TString cutname);
        bool AddCut(jFunc func);
        bool FillEvt(TH1* rhist, TH1* whist);
        bool CutEvt(int index, float weight, int var);
        bool Initialize();
        bool Print(TH1* rhist, TH1* whist);

    private:

        int numcuts_;
        TString cutflowname_;

        jCutVec *cuts_;
};

#endif
