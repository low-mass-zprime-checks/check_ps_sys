#ifndef jxAOD_jxDAOD_Truth_4mu_H
#define jxAOD_jxDAOD_Truth_4mu_H

#include <AnaAlgorithm/AnaAlgorithm.h>

#include "jxAOD/jCutFlow.h"
#include "jxAOD/Counting.h"
#include "AsgTools/AnaToolHandle.h"
//#include "PMGTools/PMGTruthWeightTool.h"

#include <TH1.h>
#include "TMatrixDSym.h"
#include <TMatrixDfwd.h>
//#include "math/matrix/src/TMatrixT.cxx"
#include "TMatrix.h"
#include "TVectorD.h"
//#include "jxAOD/ObjectResolutions.h"

#include "jxAOD/objres/JERData16EMTopo.h"
#include "jxAOD/objres/JERMC16EMTopo.h"

#include "jxAOD/objres/jetphi20.h"
#include "jxAOD/objres/jetphi50.h"
#include "jxAOD/objres/jetphi100.h"

#include "jxAOD/objres/r1_ID_MC_BARREL.h"
#include "jxAOD/objres/r2_ID_MC_BARREL.h"
#include "jxAOD/objres/r0_MS_MC_BARREL.h"
#include "jxAOD/objres/r1_MS_MC_BARREL.h"
#include "jxAOD/objres/r2_MS_MC_BARREL.h"

#include "jxAOD/objres/r1_ID_MC_ENDCAP.h"
#include "jxAOD/objres/r2_ID_MC_ENDCAP.h"
#include "jxAOD/objres/r0_MS_MC_ENDCAP.h"
#include "jxAOD/objres/r1_MS_MC_ENDCAP.h"
#include "jxAOD/objres/r2_MS_MC_ENDCAP.h"

#include "jxAOD/objres/electronSampling90.h"
#include "jxAOD/objres/electronNoise90.h"
#include "jxAOD/objres/electronConst90.h"

#include "jxAOD/objres/photonSampling90.h"
#include "jxAOD/objres/photonNoise90.h"
#include "jxAOD/objres/photonConst90.h"

#include "jxAOD/objres/tauRes1p0nBin0.h"
#include "jxAOD/objres/tauRes1p0nBin1.h"
#include "jxAOD/objres/tauRes1p0nBin2.h"
#include "jxAOD/objres/tauRes1p0nBin3.h"
#include "jxAOD/objres/tauRes1p0nBin4.h"
//#include "TMatrixDfwd.h"

class jxDAOD_Truth_4mu : public EL::AnaAlgorithm
{
    public:
        // this is a standard algorithm constructor
        jxDAOD_Truth_4mu (const std::string& name, ISvcLocator* pSvcLocator);

        // these are the functions inherited from Algorithm
        virtual StatusCode initialize () override;
        virtual StatusCode execute () override;
        virtual StatusCode finalize () override;
        virtual StatusCode fileExecute() override;
        static bool sortTruthParticlePt (const xAOD::TruthParticle* i ,const xAOD::TruthParticle*j);
        static bool sortTLVPt (TLorentzVector  i ,TLorentzVector j);
        static void rotateXY(TMatrix &mat, TMatrix &mat_new,double phi);
        double getHistValue(double *histdata, double x, double y, bool interpolateX);
        double getJetPtResolution(TLorentzVector& obj);
        double getJetPhiResolution(TLorentzVector& obj);
        double getMuonPtResolution(TLorentzVector& obj);
        double getElectronEtResolution(TLorentzVector& obj);
        double getPhotonEtResolution(TLorentzVector& obj);
        double getTauPtResolution(TLorentzVector& obj);
        void getObjectResolution(TLorentzVector& obj, TString object_type, double& pt_reso, double& phi_reso);
           
    long m_eventCounter;

    private:
        xAOD::TEvent *m_event;

        jCutFlow *cutflow;

        uint64_t nAcceptedEvents;
        double   sumOfEventWeights;
        double   sumOfEventWeightsSquared;
        //double   weight_pdf;
        double   weight;
        //std::shared_ptr<ObjectResolutions> m_objRes;

        std::vector<double> vweight;
        std::vector<std::string> iKey;
        std::vector<std::string> dKey;
        std::vector<std::string> vKey;
        std::map<std::string, int> iMap;
        std::map<std::string, double> dMap;
        std::map<std::string, std::vector<float>*> vMap;
        
        // Configuration, and any other types of variables go here.
        //float m_cutValue;
        //TTree *m_myTree;
        //TH1 *m_myHist;
};

#endif
