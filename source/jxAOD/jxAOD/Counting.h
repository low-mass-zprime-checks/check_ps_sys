#ifndef Counting_h
#define Counting_h

#include <string>
#include <vector>
#include <math.h>
#include <iostream>

using namespace std;

//#include "xAODJet/JetContainer.h"
//#include "xAODMuon/Muon.h"
//#include <xAODMuon/MuonContainer.h>
//#include <xAODMuon/MuonAuxContainer.h>
//#include "xAODEgamma/Electron.h"
//#include "jVBSZZ/AnalysisVar.h"
#include <xAODTruth/TruthParticle.h>
#include <xAODTruth/TruthParticleContainer.h>

bool CheckFromZ(const xAOD::TruthParticle* trPart, int PDG) ;

bool CheckFromZ_MG(const xAOD::TruthParticle* trPart, int PDG) ;

bool CheckFromHadron(const xAOD::TruthParticle* trPart) ;
#endif
