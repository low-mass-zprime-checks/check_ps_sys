#include "jxAOD/jCutFlow.h"

#include <AsgTools/MessageCheck.h>

#include <iostream>
#include <iomanip>

jCut::jCut(jFunc func, TString cutname, int index){
    this->func_ = func;
    this->cutname_ = cutname;
    this->index_ = index;
    this->pass_ = false;
    this->weight_ = 0.0;
}


jCut::jCut(jFunc func){
    this->func_ = func;
    this->cutname_ = "";
    this->index_ = -1;
    this->pass_ = false;
    this->weight_ = 0.0;
}


jCutFlow::jCutFlow(TString cutflowname){
    ANA_MSG_INFO ("init cutflow ");
    this->cutflowname_ = cutflowname;
    this->numcuts_ = 0;
    this->cuts_ = new jCutVec;
    this->cuts_->clear();
    ANA_MSG_INFO ("init cutflow ");
    this->AddCut([](int i){return true;}, "DAOD");
    ANA_MSG_INFO ("init cutflow ");
}


bool jCutFlow::AddCut(jFunc func){
    jCut *cut = new jCut(func);
    this->cuts_->push_back(cut);
    return true;
}


bool jCutFlow::AddCut(jFunc func, TString cutname){
    ANA_MSG_INFO ("add cut");
    jCut *cut = new jCut(func, cutname, ++(this->numcuts_));
    this->cuts_->push_back(cut);
    ANA_MSG_INFO( "CUT index " << cut->Index());
    return true;
}


bool jCutFlow::Initialize(){
    this->numcuts_ = this->cuts_->size();
    ANA_MSG_INFO ("number cuts " << this->numcuts_);
    return true;
}


bool jCutFlow::CutEvt(int index, float weight, int var){
    jCut *cut = (*this->cuts_)[index];
    cut->SetPass( cut->Func() (var) );
    cut->SetWeight( weight );
    //ANA_MSG_INFO ("var " << var << " weight " << weight << "pass "<< cut->Pass());
    return true;
}


bool jCutFlow::FillEvt(TH1* rhist, TH1* whist){
    for (auto cut : *this->cuts_){
        //ANA_MSG_INFO("LOOP CUT INDEX " << cut->Index());
        if (!cut->Pass()) break;
        if (cut->Index() != -1){
            //ANA_MSG_INFO("FILL CUT INDEX " << cut->Index()<< "  weight "<< cut->Weight());
            rhist->Fill(cut->Index());
            whist->Fill(cut->Index(), cut->Weight());
        }
    }
    return true;
}

bool jCutFlow::Print(TH1* rhist, TH1* whist){
    for (auto cut : *this->cuts_){
        if (cut->Index() != -1){
            std::cout
                << std::setw(20) << cut->Name()
                << std::setw(20) << cut->Index()
                << std::setw(20) << rhist->GetBinContent(cut->Index()+1)
                << std::setw(20) << whist->GetBinContent(cut->Index()+1)
                << std::endl;
        }
    }
    return true;
}
