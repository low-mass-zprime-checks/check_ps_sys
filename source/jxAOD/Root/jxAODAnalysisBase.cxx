#include <AsgTools/MessageCheck.h>
#include <jxAOD/jxAODAnalysisBase.h>

#include <xAODEventInfo/EventInfo.h>

#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"
#include "xAODMetaData/FileMetaData.h"

#include "xAODJet/JetContainer.h"
#include "xAODTruth/TruthEvent.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"

#include "EventLoop/Job.h"
#include "EventLoop/StatusCode.h"
#include "EventLoop/Worker.h"
#include "EventLoop/OutputStream.h"

#include "TTree.h"
#include "TFile.h"
#include "TString.h"

#include <vector>


jxAODAnalysisBase :: jxAODAnalysisBase (const std::string& name,
        ISvcLocator *pSvcLocator)
: AnaAlgorithm (name, pSvcLocator)
{
    // Here you put any code for the base initialization of variables,
    // e.g. initialize all pointers to 0.  This is also where you
    // declare all properties for your algorithm.  Note that things like
    // resetting statistics variables or booking histograms should
    // rather go into the initialize() function.
}



StatusCode jxAODAnalysisBase :: initialize ()
{

    ANA_MSG_INFO("In initialize()");
    ANA_CHECK(requestFileExecute());

    cutflow = new jCutFlow("event");
    //ANA_CHECK(cutflow->AddCut([](int a){return a>500;}, "n500"));
    //ANA_CHECK(cutflow->AddCut([](int a){return a>1000;}, "n1000"));
    ANA_CHECK(cutflow->Initialize());
    ANA_CHECK(book (TH1F("wnumber", "number", 3, 0, 3)));
    ANA_CHECK(book (TH1F("rnumber", "number", 3, 0, 3)));
    ANA_CHECK(book (TH1F("sum_of_weight", "sum_of_weight", 200, 0, 200)));
    ANA_CHECK (book (TTree ("analysis", "My analysis ntuple")));

    iKey.clear();dKey.clear();
    iKey.push_back("event_type");
    iKey.push_back("inSR");
    iKey.push_back("nele");
    iKey.push_back("nmuo");
    iKey.push_back("ntau");
    dKey.push_back("weight");
    //dKey.push_back("weight_pdf");
    dKey.push_back("M2L");
    dKey.push_back("PtL1");
    dKey.push_back("PtL2");
    dKey.push_back("EtaL1");
    dKey.push_back("EtaL2");
    dKey.push_back("PhiL1");
    dKey.push_back("PhiL2");
    dKey.push_back("MJJ");
    dKey.push_back("dEtaJJ");
    dKey.push_back("PtJ1");
    dKey.push_back("PtJ2");
    dKey.push_back("EtaJ1");
    dKey.push_back("EtaJ2");
    dKey.push_back("PhiJ1");
    dKey.push_back("PhiJ2");
    dKey.push_back("MET");
    dKey.push_back("dLepR");
    dKey.push_back("dMetZPhi");
    dKey.push_back("dPhiJ100met");
    TTree* mytree = tree("analysis");
    for (auto key: iKey) iMap[key] = -999;
    for (auto key: dKey) dMap[key] = -999;
    for (auto key: iKey) mytree->Branch(key.c_str(), &iMap[key], (key + "/I").c_str());
    for (auto key: dKey) mytree->Branch(key.c_str(), &dMap[key], (key + "/D").c_str());

    return StatusCode::SUCCESS;
}



StatusCode jxAODAnalysisBase :: execute ()
{
    return StatusCode::SUCCESS;
    for (auto key: iKey) iMap[key] = -999;
    for (auto key: dKey) dMap[key] = -999;
    const xAOD::EventInfo *eventInfo = nullptr;
    ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));

    std::vector<float> vw;
    vw = eventInfo->mcEventWeights();
    float gen_weight = vw.size()>0?vw[0]:1.0;

    //weight = vw[0];
    //weight_pdf = 0.0;
    //for (int i=11;i <111;i++) weight_pdf += (vw[0] - vw[i]) * (vw[0] - vw[i]);
    //weight_pdf = sqrt(weight_pdf / 100);
    dMap["weight"] = vw[0];
    //dMap["weight_pdf"] = weight_pdf;

    //tree("analysis")->Fill();
    //return StatusCode::SUCCESS;
    //for (int i = 0; i<115; i++) vweight[i] += vw[i];

    ANA_CHECK(cutflow->CutEvt(0, gen_weight,  eventInfo->eventNumber()));
    //ANA_CHECK(cutflow->CutEvt(1, 0.4,  eventInfo->eventNumber()));
    //ANA_CHECK(cutflow->CutEvt(2, 0.4,  eventInfo->eventNumber()));
    ANA_CHECK(cutflow->FillEvt(hist("rnumber"), hist("wnumber")));

    //for(int i=11; i<111; i++) pdf_var += (vw[i]-vw[0])*(vw[i]-vw[0]);
    //pdf_var = sqrt(pdf_var/100.0);

    vector<TLorentzVector> vp4e, vp4m, vp4t;
    vector<TLorentzVector> vp4g, vp4j, vp4l, vp4n;
    vector<TLorentzVector> vp4n16, vp4n14, vp4n12;
    vp4e.clear(); vp4m.clear(); vp4t.clear();
    vp4g.clear(); vp4j.clear(); vp4l.clear(); vp4n.clear();
    vp4n16.clear(); vp4n14.clear(); vp4n12.clear();

    const xAOD::TruthEventContainer* TruthEvtContainer = nullptr;
    ANA_CHECK( m_event->retrieve(TruthEvtContainer, "TruthEvents"));
    for ( auto trEvtItr: *TruthEvtContainer){
        int nTruPart = trEvtItr->nTruthParticles();
        for (int j=0; j<nTruPart; j++){
            const xAOD::TruthParticle* trPart = trEvtItr->truthParticle(j);
            if(!trPart) continue;
            int status = trPart->status();
            int PDG = trPart->pdgId();
            int barcode = trPart->barcode();
            TLorentzVector p4 = trPart->p4();
            //if (! trPart->parent())
            //    ANA_MSG_INFO( "PDG " << trPart->pdgId() << ", Status " << trPart->status());
            //else
            //    ANA_MSG_INFO( "PDG " << trPart->pdgId() << ", Status " << trPart->status() << ", Par PDG "<< trPart->parent()->pdgId() << ", Status "<< trPart->parent()->status());

            //if(fabs(PDG)==11 && status==11 && barcode<=200000) if(! CheckFromHadron(trPart) && trPart->pt()>5.e3 && fabs(trPart->eta())<2.7) vp4e.push_back(p4);
            //if(fabs(PDG)==13 && status==11 && barcode<=200000) if(! CheckFromHadron(trPart) && trPart->pt()>5.e3 && fabs(trPart->eta())<2.7) vp4m.push_back(p4);
            //if(fabs(PDG)==15 && status==11 && barcode<=200000) if(! CheckFromHadron(trPart) && trPart->pt()>5.e3 && fabs(trPart->eta())<2.7) vp4t.push_back(p4);
            if(fabs(PDG)==11 && status==11 && barcode<=200000) if(! CheckFromHadron(trPart)) vp4e.push_back(p4);
            if(fabs(PDG)==13 && status==11 && barcode<=200000) if(! CheckFromHadron(trPart)) vp4m.push_back(p4);
            if(fabs(PDG)==15 && status==11 && barcode<=200000) if(! CheckFromHadron(trPart)) vp4t.push_back(p4);

            if(fabs(PDG)==12 && status==11 && barcode<=200000) if (! CheckFromHadron(trPart)) vp4n12.push_back(p4);
            if(fabs(PDG)==14 && status==11 && barcode<=200000) if (! CheckFromHadron(trPart)) vp4n14.push_back(p4);
            if(fabs(PDG)==16 && status==11 && barcode<=200000) if (! CheckFromHadron(trPart)) vp4n16.push_back(p4);

            if(fabs(PDG)==22 && status==1 && barcode<99999) if (! CheckFromHadron(trPart)) vp4g.push_back(p4); 
        }
    }

    if (vp4e.size()==0 && vp4m.size()>0 && vp4t.size()==0) {
        for (auto p4: vp4m){
            if (vp4l.size()>0) if (std::any_of(vp4l.begin(), vp4l.end(), [&](auto p4l){return p4l.DeltaR(p4)<0.1;})) continue;
            vp4l.push_back(p4);
        }
        if (vp4l.size()==2) iMap["event_type"] = 0;
        iMap["nmuo"] = vp4l.size();
    }
    if (vp4e.size()>0 && vp4m.size()==0 && vp4t.size()==0) {
        for (auto p4: vp4e){
            if (vp4l.size()>0) if (std::any_of(vp4l.begin(), vp4l.end(), [&](auto p4l){return p4l.DeltaR(p4)<0.1;})) continue;
            vp4l.push_back(p4);
        }
        if (vp4l.size()==2) iMap["event_type"] = 1;
        iMap["nele"] = vp4l.size();
    }
    if (vp4e.size()==0 && vp4m.size()==0 && vp4t.size()>0) {
        for (auto p4: vp4t){
            if (vp4l.size()>0) if (std::any_of(vp4l.begin(), vp4l.end(), [&](auto p4l){return p4l.DeltaR(p4)<0.1;})) continue;
            vp4l.push_back(p4);
        }
        if (vp4l.size()==2) iMap["event_type"] = 2;
        iMap["ntau"] = vp4l.size();
    }
        //ANA_MSG_INFO(vp4l.size());

    if (vp4n12.size()==2 && vp4n14.size()==0 && vp4n16.size()==0) { vp4n.push_back(vp4n12[0]); vp4n.push_back(vp4n12[1]); }
    if (vp4n12.size()==0 && vp4n14.size()==2 && vp4n16.size()==0) { vp4n.push_back(vp4n14[0]); vp4n.push_back(vp4n14[1]); }
    if (vp4n12.size()==0 && vp4n14.size()==0 && vp4n16.size()==2) { vp4n.push_back(vp4n16[0]); vp4n.push_back(vp4n16[1]); }

    if (vp4l.size()==2){
        // dressing photons
        for (int i =0; i<(int)vp4g.size(); i++){
            double dR0 = vp4g[i].DeltaR(vp4l[0]);
            double dR1 = vp4g[i].DeltaR(vp4l[1]);
            if (dR0<dR1 && dR0<0.1) vp4l[0] = vp4l[0] + vp4g[i];
            if (dR0>dR1 && dR1<0.1) vp4l[1] = vp4l[1] + vp4g[i];
        }

        // sorting leptons
        if (vp4l[0].Pt() > vp4l[1].Pt()){
            dMap["PtL1" ] = vp4l[0].Pt() *1.e-3;
            dMap["EtaL1"] = vp4l[0].Eta();
            dMap["PhiL1"] = vp4l[0].Phi();
            dMap["PtL2" ] = vp4l[1].Pt() *1.e-3;
            dMap["EtaL2"] = vp4l[1].Eta();
            dMap["PhiL2"] = vp4l[1].Phi();
        } else{
            dMap["PtL1" ] = vp4l[1].Pt() *1.e-3;
            dMap["EtaL1"] = vp4l[1].Eta();
            dMap["PhiL1"] = vp4l[1].Phi();
            dMap["PtL2" ] = vp4l[0].Pt() *1.e-3;
            dMap["EtaL2"] = vp4l[0].Eta();
            dMap["PhiL2"] = vp4l[0].Phi();
        }

        dMap["M2L"] = (vp4l[0] + vp4l[1]).M() *1.e-3;
        dMap["dLepR"] = vp4l[0].DeltaR(vp4l[1]);
    }

    if (vp4n.size()==2){
        dMap["MET"] = (vp4n[0] + vp4n[1]).Pt() *1.e-3;
    }
    if (vp4l.size()==2 && vp4n.size()==2){
        dMap["dMetZPhi"] = (vp4n[0] + vp4n[1]).DeltaPhi(vp4l[0] + vp4l[1]);
    }

    const xAOD::JetContainer* WZTruthJets= nullptr;
    ANA_CHECK( m_event->retrieve( WZTruthJets, "AntiKt4TruthDressedWZJets"));
    for ( auto trJet: *WZTruthJets){
        if (trJet->pt()>20.e3 && fabs(trJet->eta())<5) vp4j.push_back(trJet->p4());
    }

    int mc_jet1_index = -1,
        mc_jet2_index = -1;
    dMap["dPhiJ100met"] = 9;
    if (vp4j.size()>=2){
        for (int i=0;i<(int)vp4j.size();i++){
            if (vp4j[i].Pt()>dMap["PtJ1"]){
                dMap["PtJ2"] = dMap["PtJ1"];
                dMap["PtJ1"] = vp4j[i].Pt();
                mc_jet2_index = mc_jet1_index;
                mc_jet1_index = i;
            }else if(vp4j[i].Pt()>dMap["PtJ2"]){
                dMap["PtJ2"] = vp4j[i].Pt();
                mc_jet2_index = i;
            }
        }
        dMap["PtJ1"] *= 1.e-3;
        dMap["PtJ2"] *= 1.e-3;
        dMap["EtaJ1"] = vp4j[mc_jet1_index].Eta();
        dMap["EtaJ2"] = vp4j[mc_jet2_index].Eta();
        dMap["PhiJ1"] = vp4j[mc_jet1_index].Phi();
        dMap["PhiJ2"] = vp4j[mc_jet2_index].Phi();

        dMap["MJJ"] = (vp4j[mc_jet1_index] + vp4j[mc_jet2_index]).M() *1.e-3;
        dMap["dEtaJJ"] = fabs(dMap["EtaJ1"] - dMap["EtaJ2"]);
        if (vp4n.size()==2 && vp4j[mc_jet1_index].Pt() > 100.e3) dMap["dPhiJ100met"] = vp4j[mc_jet1_index].DeltaPhi(vp4n[0] + vp4n[1]);
    }

    std::array<bool, 6> selection = {
        iMap["event_type"]==0 || iMap["event_type"]==1,
        dMap["M2L"]>76 && dMap["M2L"]<106,
        dMap["MET"]>120,
        fabs(dMap["dLepR"])<1.8,
        fabs(dMap["dPhiJ100met"])>0.4,
        fabs(dMap["dMetZPhi"])>2.5,
    };

    if (std::all_of(selection.begin(), selection.end(), [](bool v){return v;})) iMap["inSR"] = 1;
    else iMap["inSR"] = 0;

    tree("analysis")->Fill();

    return StatusCode::SUCCESS;
}


StatusCode jxAODAnalysisBase :: fileExecute()
{
    m_event = wk()->xaodEvent();

    const xAOD::CutBookkeeperContainer* completeCBC(nullptr);
    ANA_CHECK(m_event->retrieveMetaInput(completeCBC, "CutBookkeepers"));

    std::map<std::string, int> cycle;
    std::map<std::string, double> sumofweight;

    for ( auto cbk :  *completeCBC ) {
        ANA_MSG_INFO(
                "cbk->name() = " << cbk->name() <<
                ", cbk->cycle() = " << cbk->cycle() <<
                ", cbk->inputStream() = " << cbk->inputStream() <<
                ", cbk->sumOfEventWeights() = " << cbk->sumOfEventWeights()
                );
        if ( TString(cbk->name()).Contains("AllExecutedEvents") && cbk->inputStream() == "StreamAOD"){
            if (cycle.find(cbk->name()) == cycle.end()){
                cycle.insert(std::pair<std::string, int>(cbk->name(), cbk->cycle()));
                sumofweight.insert(std::pair<std::string, double>(cbk->name(), cbk->sumOfEventWeights()));
            }else{
                if (cbk->cycle() > cycle[cbk->name()]) {
                    cycle[cbk->name()] = cbk->cycle();
                    sumofweight[cbk->name()] = cbk->sumOfEventWeights();
                }
            }
        }
    }

    for (int i =0; i<sumofweight.size(); i++){
        std::string key;
        if (i==0) key = "AllExecutedEvents";
        else key = "AllExecutedEvents_NonNominalMCWeight_" + std::to_string(i);
        hist("sum_of_weight")->Fill(i, sumofweight[key]);
    }

    return StatusCode::SUCCESS;
}

StatusCode jxAODAnalysisBase :: finalize ()
{
    ANA_CHECK(cutflow->Print(hist("rnumber"), hist("wnumber")));
    return StatusCode::SUCCESS;
}
