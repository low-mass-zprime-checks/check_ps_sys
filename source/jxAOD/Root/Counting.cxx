#include <string>
#include <vector>
#include <math.h>
#include <iostream>

#include "jxAOD/Counting.h"

using namespace std;

bool CheckFromZ(const xAOD::TruthParticle* trPart, int PDG) {
     const xAOD::TruthParticle * parent = trPart->parent();
     if (!parent){return false;}
     if (parent->pdgId()==PDG && parent->status()!=11) return CheckFromZ(parent, PDG);

     size_t nParents = trPart->nParents();
     bool findZ=false;
     for(int i=0; i<(int)nParents; i++) {
        const xAOD::TruthParticle* parPart_3 = trPart->parent(i);
        int pdg_3 = parPart_3->pdgId();
        int status_3 = parPart_3->status();
        if(pdg_3==PDG && status_3==11) findZ=true;
     }
     if(findZ) return true;
     else return false;
}

bool CheckFromZ_MG(const xAOD::TruthParticle* trPart, int PDG) {
     const xAOD::TruthParticle * parent = trPart->parent();
     if (!parent){return false;}
     if (parent->pdgId()==PDG) return CheckFromZ_MG(parent, PDG);
     size_t nParents = trPart->nParents();
     bool findZ=false;
     for(int i=0; i<(int)nParents; i++) {
        const xAOD::TruthParticle* parPart_3 = trPart->parent(i);
        if(parPart_3->pdgId()==23 && (parPart_3->status()==62 || parPart_3->status()==22)) findZ=true;
     }
     if(findZ) return true;
     else return false;
}


bool CheckFromHadron(const xAOD::TruthParticle* trPart) {
     if(!trPart->parent()) return false;

     bool const parent_NOT_in_initial_state = (trPart->parent()->status() != 4 );
     bool const parent_NOT_in_hard_process  = (trPart->parent()->status() != 11);

     if(trPart->parent()->isHadron() &&
        parent_NOT_in_initial_state && parent_NOT_in_hard_process) return true;

     return CheckFromHadron(trPart->parent());

}
