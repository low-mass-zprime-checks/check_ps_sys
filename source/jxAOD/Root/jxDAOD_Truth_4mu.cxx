#include <iostream>
#include <random>
#include <vector>

#include <AsgTools/MessageCheck.h>
#include <jxAOD/jxDAOD_Truth_4mu.h>

#include <xAODEventInfo/EventInfo.h>

#include "xAODMetaData/FileMetaData.h"

#include "xAODTruth/TruthEvent.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"

#include "EventLoop/Job.h"
#include "EventLoop/StatusCode.h"
#include "EventLoop/Worker.h"
#include "EventLoop/OutputStream.h"

#include "TTree.h"
#include "TFile.h"
#include "TString.h"
#include "TMath.h"

std::mt19937 gen(24);
double MeVtoGev = 0.001;
double Z_mass = 91.1876;

jxDAOD_Truth_4mu ::jxDAOD_Truth_4mu(const std::string& name, ISvcLocator* pSvcLocator)
    : AnaAlgorithm(name, pSvcLocator) {
    // Here you put any code for the base initialization of variables,
    // e.g. initialize all pointers to 0.  This is also where you
    // declare all properties for your algorithm.  Note that things like
    // resetting statistics variables or booking histograms should
    // rather go into the initialize() function.
}

StatusCode jxDAOD_Truth_4mu ::initialize() {
    ANA_MSG_INFO("In initialize()");
    ANA_CHECK(requestFileExecute());
    ANA_CHECK(book(TTree("analysis", "My analysis ntuple")));

    iKey = {"pass_selection"};
    dKey = {"weight", "pass_weight", "l1_pt",  "l2_pt",  "l3_pt",  "l4_pt",  "l1_eta", "l2_eta",
            "l3_eta", "l4_eta", "l1_phi", "l2_phi", "l3_phi", "l4_phi"};
    vKey = {};
    TTree* mytree = tree("analysis");
    for (auto key : iKey) iMap[key] = -999;
    for (auto key : dKey) dMap[key] = -999;
    for (auto key : vKey) vMap[key] = new std::vector<float>();
    for (auto key : iKey) mytree->Branch(key.c_str(), &iMap[key]);
    for (auto key : dKey) mytree->Branch(key.c_str(), &dMap[key]);
    for (auto key : vKey) mytree->Branch(key.c_str(), &vMap[key]);
    m_eventCounter = 0;

    return StatusCode::SUCCESS;
}

StatusCode jxDAOD_Truth_4mu ::execute() {
    // shoe progress
    if ((m_eventCounter % 10000) == 0) {
        printf("execute(), Event number = %li\n", m_eventCounter);
    }
    ++m_eventCounter;

    // Get weights
    const xAOD::EventInfo* eventInfo = nullptr;
    ANA_CHECK(evtStore()->retrieve(eventInfo, "EventInfo"));
    std::vector<float> vw = eventInfo->mcEventWeights();
    dMap["weight"] = vw[0];

    // Prepare objects to save data
    vector<TLorentzVector> vp4g, vp4m;
    vector<int> charge_4m;

    // Get truth data
    const xAOD::TruthEventContainer* TruthEvtContainer = nullptr;
    ANA_CHECK(evtStore()->retrieve(TruthEvtContainer, "TruthEvents"));
    int number_events = 0;
    for (auto trEvtItr : *TruthEvtContainer) {
        number_events++;
        int nTruPart = trEvtItr->nTruthParticles();
        const xAOD::TruthParticleContainer* TruthEle = nullptr;
        ANA_CHECK(evtStore()->retrieve(TruthEle, "TruthElectrons"));
        const xAOD::TruthParticleContainer* TruthMu = nullptr;
        ANA_CHECK(evtStore()->retrieve(TruthMu, "TruthMuons"));
        const xAOD::TruthParticleContainer* TruthTau = nullptr;
        ANA_CHECK(evtStore()->retrieve(TruthTau, "TruthTaus"));
        const xAOD::TruthParticleContainer* TruthNeutr = nullptr;
        ANA_CHECK(evtStore()->retrieve(TruthNeutr, "TruthNeutrinos"));
        const xAOD::TruthParticleContainer* TruthGamma = nullptr;
        ANA_CHECK(evtStore()->retrieve(TruthGamma, "TruthPhotons"));
        std::vector<const xAOD::TruthParticle*> TruthPart;
        for (auto e : *TruthEle) TruthPart.push_back(e);
        for (auto mu : *TruthMu) TruthPart.push_back(mu);
        for (auto t : *TruthTau) TruthPart.push_back(t);
        for (auto n : *TruthNeutr) TruthPart.push_back(n);
        for (auto g : *TruthGamma) TruthPart.push_back(g);
        // get 4-vector
        for (auto trPart : TruthPart) {
            int status = trPart->status();
            int PDG = trPart->pdgId();
            int barcode = trPart->barcode();
            TLorentzVector p4;  // = trPart->p4();
            p4.SetPtEtaPhiE(trPart->pt() * MeVtoGev, trPart->eta(), trPart->phi(), trPart->e() * MeVtoGev);
            // sherpa
            if (fabs(PDG) == 13 && status == 1 && barcode <= 200000)
                if ((!CheckFromHadron(trPart)) && (fabs(p4.Eta()) < 2.9)) {
                    vp4m.push_back(p4);
                    if (PDG > 0) {
                        charge_4m.push_back(1);

                    } else {
                        charge_4m.push_back(-1);
                    }
                }
            if (fabs(PDG) == 22 && status == 11 && barcode < 99999)
                if ((!CheckFromHadron(trPart)) && (fabs(p4.Eta()) < 2.4)) {
                    vp4g.push_back(p4);
                }
        }
    }

    // Only use 4mu events
    if (vp4m.size() != 4) {
        return StatusCode::SUCCESS;
    }

    // Calculate necessary variables to apply cuts
    if (vp4m.size() == 4) {
        // dressing photons
        for (int i = 0; i < (int)vp4g.size(); i++) {
            double dR0 = vp4g[i].DeltaR(vp4m[0]);
            double dR1 = vp4g[i].DeltaR(vp4m[1]);
            if (dR0 < dR1 && dR0 < 0.1) vp4m[0] = vp4m[0] + vp4g[i];
            if (dR0 > dR1 && dR1 < 0.1) vp4m[1] = vp4m[1] + vp4g[i];
        }
        // sort & save leptons
        std::sort(vp4m.begin(), vp4m.end(),
                  [](const TLorentzVector& p1, const TLorentzVector& p2) { return p1.Pt() > p2.Pt(); });
        dMap["l1_pt"] = vp4m[0].Pt();
        dMap["l1_eta"] = vp4m[0].Eta();
        dMap["l1_phi"] = vp4m[0].Phi();
        dMap["l2_pt"] = vp4m[1].Pt();
        dMap["l2_eta"] = vp4m[1].Eta();
        dMap["l2_phi"] = vp4m[1].Phi();
        dMap["l3_pt"] = vp4m[2].Pt();
        dMap["l3_eta"] = vp4m[2].Eta();
        dMap["l3_phi"] = vp4m[2].Phi();
        dMap["l4_pt"] = vp4m[3].Pt();
        dMap["l4_eta"] = vp4m[3].Eta();
        dMap["l4_phi"] = vp4m[3].Phi();
    }

    // Apply cuts & construct lepton pairs and quadruplets

    // check muons objects
    bool pass_cut = true;
    if (pass_cut) {
        for (int i = 0; i < (int)vp4m.size(); i++) {
            if (!(vp4m[i].Pt() > 3 && fabs(vp4m[i].Eta()) < 2.7)) {
                pass_cut = false;
                break;
            }
        }
    }

    // get muon pairs
    vector<TLorentzVector> vp_lep_pair;
    vector<vector<int>> vp_lep_pair_id;
    if (pass_cut) {
        for (int i = 0; i < 4; i++) {
            for (int j = i + 1; j < 4; j++) {
                TLorentzVector v_pair = vp4m[i] + vp4m[j];
                if (v_pair.M() > 4 && charge_4m[i] * charge_4m[j] < 0) {
                    vp_lep_pair.push_back(v_pair);
                    vector<int> muon_pair_id;
                    muon_pair_id.push_back(i);
                    muon_pair_id.push_back(j);
                    vp_lep_pair_id.push_back(muon_pair_id);
                }
            }
        }
        if (vp_lep_pair.size() < 2) {
            pass_cut = false;
        }
    }

    // get muon quadruplets
    if (pass_cut) {
        int z1_choice = -1;
        double min_diff = 100000;
        for (int i = 0; i < vp_lep_pair.size(); i++) {
            double cur_diff = fabs(vp_lep_pair[i].M() - Z_mass);
            if (cur_diff < min_diff) {
                min_diff = cur_diff;
                z1_choice = i;
            }
        }
        int z2_choice = -1;
        double max_mass = 0;
        if (z1_choice != -1) {
            int id1 = vp_lep_pair_id[z1_choice][0];
            int id2 = vp_lep_pair_id[z1_choice][1];
            for (int i = 0; i < vp_lep_pair.size(); i++) {
                if (i != z1_choice) {
                    int id3 = vp_lep_pair_id[i][0];
                    int id4 = vp_lep_pair_id[i][1];
                    if (id1 != id3 && id1 != id4 && id2 != id3 && id2 != id4) {
                        double cur_mass = vp_lep_pair[i].M();
                        if (cur_mass > max_mass) {
                            max_mass = cur_mass;
                            z2_choice = i;
                        }
                    }
                }
            }
        }
        if (z1_choice != -1 && z2_choice != -1) {
            TLorentzVector lep_1 = vp_lep_pair[vp_lep_pair_id[z1_choice][0]];
            TLorentzVector lep_2 = vp_lep_pair[vp_lep_pair_id[z1_choice][1]];
            TLorentzVector lep_3 = vp_lep_pair[vp_lep_pair_id[z2_choice][0]];
            TLorentzVector lep_4 = vp_lep_pair[vp_lep_pair_id[z2_choice][1]];
            // pT requirement
            if (!(lep_1.Pt() > 20 and lep_2.Pt() > 15 and lep_3.Pt() > 8)) {
                pass_cut = false;
            }
            // delta R requirement
            if (lep_1.DeltaR(lep_2) < 0.2) {
                pass_cut = false;
            } else if (lep_1.DeltaR(lep_3) < 0.2) {
                pass_cut = false;
            } else if (lep_1.DeltaR(lep_4) < 0.2) {
                pass_cut = false;
            } else if (lep_2.DeltaR(lep_3) < 0.2) {
                pass_cut = false;
            } else if (lep_2.DeltaR(lep_4) < 0.2) {
                pass_cut = false;
            } else if (lep_3.DeltaR(lep_4) < 0.2) {
                pass_cut = false;
            }
        } else {
            pass_cut = false;
        }
    }

    // check inv. mass
    if (pass_cut) {
        TLorentzVector v_4mu = vp4m[0] + vp4m[1] + vp4m[2] + vp4m[3];
        double m_4mu = v_4mu.M();
        if (!((m_4mu > 80 && m_4mu < 110) || (m_4mu > 130 && m_4mu < 180))) {
            pass_cut = false;
        }
    }

    // save cut status
    if (pass_cut) {
        iMap["pass_selection"] = 1;
        dMap["pass_weight"] = dMap["weight"];
    } else {
        iMap["pass_selection"] = 0;
        dMap["pass_weight"] = 0;
    }

    tree("analysis")->Fill();

    return StatusCode::SUCCESS;
}

StatusCode jxDAOD_Truth_4mu ::fileExecute() { return StatusCode::SUCCESS; }

StatusCode jxDAOD_Truth_4mu ::finalize() { return StatusCode::SUCCESS; }
