#!/usr/bin/env python

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'submitDir',
                                      help = 'Submission directory for EventLoop' )
( options, args ) = parser.parse_args()

# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
import sys
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )
#ROOT.SH.ScanDir().filePattern( sys.argv[1] ).scan( sh, inputFilePath )
#path = '/lustre/umt3/user/liji/DAODs/DAOD_HIGG2D1/mc16_13TeV.364250.Sherpa_222_NNPDF30NNLO_llll.deriv.DAOD_HIGG2D1.e5894_s3126_r9364_p3654'
#path = '/lustre/umt3/user/liji/DAODs/DAOD_HIGG2D1/mc16_13TeV.345666.Sherpa_222_NNPDF30NNLO_llvvZZ.deriv.DAOD_HIGG2D1.e6240_s3126_r10201_p3652'
#path = sys.argv[1]
#f = 'DAOD_HIGG2D1.15833224._000231.pool.root.1'
f = '*.pool.root.1'
#ROOT.SH.ScanDir().filePattern( f ).scan( sh, path)
ROOT.SH.scanRucio().scan( sh, 'mc16_13TeV.345666.Sherpa_222_NNPDF30NNLO_llvvZZ.recon.AOD.e6240_s3126_r9364')

sh.printContent()

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
#job.options().setDouble( ROOT.EL.Job.optMaxEvents, 30)

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ( 'jxAODAnalysisBase', 'AnalysisAlg' )

job.outputAdd (ROOT.EL.OutputStream ('ANALYSIS'))

# later on we'll add some configuration options for our algorithm that go here

# Add our algorithm to the job
job.algsAdd( alg )

# Run the job using the direct driver.
driver = ROOT.EL.PrunDriver()
driver.options().setString('nc_outputSampleName', 'user.liji.test.0523')
#driver.submitOnly( job, options.submission_dir )
driver.submitOnly( job, 'user.liji.test.0523')

