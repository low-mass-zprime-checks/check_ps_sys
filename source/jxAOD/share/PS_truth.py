#!/usr/bin/env python

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
import os
import shutil

parser = optparse.OptionParser()
parser.add_option(
    "-s",
    "--submission-dir",
    dest="submission_dir",
    action="store",
    type="string",
    default="submitDir",
    help="Submission directory for EventLoop",
)
parser.add_option(
    "-f",
    "--force",
    action="store_true",
    default=False,
    help="Force deletion of the output folder",
)
parser.add_option(
    "-n",
    "--max_events",
    action="store",
    type=int,
    default=-1,
    help="Maximum number of events to process, use -1 to run all"
)
(options, args) = parser.parse_args()


# Set up (Py)ROOT.
import ROOT

ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
sh = ROOT.SH.SampleHandler()
sh.setMetaString("nc_tree", "CollectionTree")

path = args[0]
output_folder = args[1]

# delete output 
if options.force and os.path.exists(output_folder):
    print "## Removing folder", output_folder
    shutil.rmtree(output_folder)

# ROOT.SH.ScanDir().filePattern( f ).scan( sh, path)
ROOT.SH.ScanDir().scan(sh, path)

#sh.printContent()

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler(sh)

# Set max events
if options.max_events >= 0:
    job.options().setDouble(ROOT.EL.Job.optMaxEvents, options.max_events)

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm("jxDAOD_Truth_4mu", "AnalysisAlg")
job.outputAdd(ROOT.EL.OutputStream("ANALYSIS"))

# later on we'll add some configuration options for our algorithm that go here

# Add our algorithm to the job
job.algsAdd(alg)

# Run the job using the direct driver.
driver = ROOT.EL.DirectDriver()
driver.submit(job, os.path.basename(args[1]))
# driver.submit( job, 'DAOD_TRUTH1.18113387._000005.pool.root.1')
