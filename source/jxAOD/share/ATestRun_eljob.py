#!/usr/bin/env python

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
import os
import sys
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'submitDir',
                                      help = 'Submission directory for EventLoop' )
( options, args ) = parser.parse_args()

# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )

path = '/lustre/umt3/user/liji/DAODs/DAOD_TRUTH1/mc15_13TeV.345666.Sherpa_222_NNPDF30NNLO_llvvZZ.deriv.DAOD_TRUTH1.e6240_e5984_p3655'
f = 'DAOD_TRUTH1.18113387._000005.pool.root.1'

#path = '/lustre/umt3/user/liji/DAODs/DAOD_HIGG2D1/mc16_13TeV.345666.Sherpa_222_NNPDF30NNLO_llvvZZ.deriv.DAOD_HIGG2D1.e6240_s3126_r10201_p3652'
#f = 'DAOD_HIGG2D1.15833224._000231.pool.root.1'

#path = '/lustre/umt3/user/liji/DAODs/DAOD_STDM3/mc16_13TeV.363724.MadGraphPythia8EvtGen_ZZllvv2jEWK.deriv.DAOD_STDM3.e5712_s3126_r10201_p3761'
#f = 'DAOD_STDM3.17244182._000001.pool.root.1'

path = sys.argv[1]
f = sys.argv[2]
#path = '/lustre/umt3/user/liji/DAODs/jun'
#f = 'R1_DAOD_TRUTH1.test_300.pool.root'

ROOT.SH.ScanDir().filePattern( f ).scan( sh, path)

sh.printContent()

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
#job.options().setDouble( ROOT.EL.Job.optMaxEvents, 1000)
#job.options().setDouble( ROOT.EL.Job.optMaxEvents, 20)

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ( 'jxDAOD_Truth_4mu', 'AnalysisAlg' )

job.outputAdd (ROOT.EL.OutputStream ('ANALYSIS'))

# later on we'll add some configuration options for our algorithm that go here

# Add our algorithm to the job
job.algsAdd( alg )

# Run the job using the direct driver.
driver = ROOT.EL.DirectDriver()
#driver.submit( job, options.submission_dir )
driver.submit( job, os.path.basename(sys.argv[2]))

