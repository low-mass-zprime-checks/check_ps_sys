void ATestRun_eljob (const std::string& submitDir)
{
    // Set up the job for xAOD access:
    xAOD::Init().ignore();

    // create a new sample handler to describe the data files we use
    SH::SampleHandler sh;

    // scan for datasets in the given directory
    // this works if you are on lxplus, otherwise you'd want to copy over files
    // to your local machine and use a local path.  if you do so, make sure
    // that you copy all subdirectories and point this to the directory
    // containing all the files, not the subdirectories.

    // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
    //const char* inputFilePath = gSystem->ExpandPathName ("/lustre/umt3/user/liji/DAODs/DAOD_HIGG2D1/mc16_13TeV.345666.Sherpa_222_NNPDF30NNLO_llvvZZ.recon.AOD.e6240_s3126_r9364");
    const char* inputFilePath = gSystem->ExpandPathName ("/lustre/umt3/user/liji/DAODs/DAOD_TRUTH1/mc15_13TeV.345666.Sherpa_222_NNPDF30NNLO_llvvZZ.deriv.DAOD_TRUTH1.e6240_e5984_p3655");
    SH::ScanDir().filePattern("*.pool.root.1").scan(sh,inputFilePath);


    // set the name of the tree in our files
    // in the xAOD the TTree containing the EDM containers is "CollectionTree"
    sh.setMetaString ("nc_tree", "CollectionTree");

    // further sample handler configuration may go here

    // print out the samples we found
    sh.print ();

    // this is the basic description of our job
    EL::Job job;
    job.sampleHandler (sh); // use SampleHandler in this job
    //job.options()->setDouble (EL::Job::optMaxEvents, 500); // for testing purposes, limit to run over the first 500 events only!
    job.outputAdd (EL::OutputStream ("ANALYSIS"));

    // add our algorithm to the job
    EL::AnaAlgorithmConfig alg;
    alg.setType ("jxDAOD_Truth_4mu");

    // set the name of the algorithm (this is the name use with
    // messages)
    alg.setName ("AnalysisAlg");

    // later on we'll add some configuration options for our algorithm that go here

    job.algsAdd (alg);

    // make the driver we want to use:
    // this one works by running the algorithm directly:
    EL::DirectDriver driver;
    // we can use other drivers to run things on the Grid, with PROOF, etc.

    // process the job using the driver
    driver.submit (job, submitDir);
}
