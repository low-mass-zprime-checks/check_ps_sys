#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "SampleHandler/DiskListLocal.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "EventLoopGrid/PrunDriver.h"

#include <PATInterfaces/SystematicCode.h>
#include "PATInterfaces/CorrectionCode.h"
#include "PATInterfaces/SystematicsUtil.h"

#include "jxAOD/jxAODAnalysisBase.h"

#include <iostream>     // std::cout
#include <fstream> 

using namespace std;

int main(int argc, char* argv[] ) {

   std::string submitDir = "";
   std::string dataset = "";
   std::string output = "";
   if( argc > 1 ) dataset = argv[ 1 ];
   if( argc > 2 ) output = argv[ 2 ];
   //if( argc > 3 ) output = argv[ 3 ];

   // Set up the job for xAOD access:
   xAOD::Init().ignore();
   
   // Construct the samples to run on:
   SH::SampleHandler sh;
   SH::scanRucio (sh, dataset);

   // Set the name of the input TTree. It's always "CollectionTree"
   // for xAOD files.
   sh.setMetaString( "nc_tree", "CollectionTree" );
   
   // Print what we found:
   sh.print();
   
   // Create an EventLoop job:
   EL::Job job;
   job.sampleHandler( sh );

   EL::AnaAlgorithmConfig alg;
   alg.setType ("jxAODAnalysisBase");
   alg.setName ("AnalysisAlg");
 
   job.algsAdd( alg );
   
   EL::PrunDriver driver;
   driver.options()->setString("nc_outputSampleName", output);
   //driver.options()->setString(EL::Job::optGridNFilesPerJob, "MAX"); //By default, split in as few jobs as possible
   //driver.options()->setDouble("nc_nFilesPerJob", 5); 
   driver.options()->setDouble("nc_mergeOutput", 0);  
   //sh.get("mc14_13TeV.110401.PowhegPythia_P2012_ttbar_nonallhad.merge.DAOD_STDM4.e2928_s1982_s2008_r5787_r5853_p1807/")->SetMetaDouble(EL::Job::optGridNFilesPerJob, 1); //For this particular sample, split into one job per input file
   //driver.options()->setDouble(EL::Job::optGridMergeOutput, 1); //run merging jobs for all samples before downloading (recommended) 
   driver.submitOnly (job, output);

    return 0;
}
