source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
sample_path=$1
job_name=$2

# go to root directory to setup environment
cd {}
source setup.sh

# go to run folder to submit jobs
cd {}
dataset=$1
name=$2

#PS_truth.py -f -n 200000 $sample_path $job_name
PS_truth.py -f $sample_path $job_name
