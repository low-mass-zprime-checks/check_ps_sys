from condor_utils import *

if __name__ == "__main__":

    job_meta = {
        "input_folder": "/net/s3_datae/yangz/zprime/DAOD",
        "output_folder": "/net/s3_data_home/yangz/analysis/zprime/check_ps_sys/run/results",
        "condor_folder": "/net/s3_data_home/yangz/analysis/zprime/check_ps_sys/run/jobs",
        "identifier": "job_21_11_10",
    }

    sample_dict = {
        "run_364250": "mc15_13TeV.364250.Sherpa_222_NNPDF30NNLO_llll.deriv.DAOD_TRUTH3.e5894_e5984_p4675",
        "run_364256": "mc15_13TeV.364256.Sherpa_222_NNPDF30NNLO_llll_CKKW15.deriv.DAOD_TRUTH3.e5917_p4675",
        "run_364257": "mc15_13TeV.364257.Sherpa_222_NNPDF30NNLO_llll_CKKW30.deriv.DAOD_TRUTH3.e5941_p4675",
        "run_364258": "mc15_13TeV.364258.Sherpa_222_NNPDF30NNLO_llll_QSF025.deriv.DAOD_TRUTH3.e5917_p4675",
        "run_364259": "mc15_13TeV.364259.Sherpa_222_NNPDF30NNLO_llll_QSF4.deriv.DAOD_TRUTH3.e5917_p4675",
        "run_364260": "mc15_13TeV.364260.Sherpa_222_NNPDF30NNLO_llll_CSSKIN.deriv.DAOD_TRUTH3.e5917_p4675",
    }

    prepare_jobs(job_meta, sample_dict)
