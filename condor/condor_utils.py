import os


def prepare_jobs(job_meta, sample_dict):
    # Prepare folders
    output_folder = os.path.join(job_meta["output_folder"], job_meta["identifier"])
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    condor_folder = os.path.join(job_meta["condor_folder"], job_meta["identifier"])
    if not os.path.exists(condor_folder):
        os.makedirs(condor_folder)
    log_folder = os.path.join(condor_folder, "logs")
    if not os.path.exists(log_folder):
        os.makedirs(log_folder)

    # Create executable
    run_tmp = open("./run_template.sh").read()
    executable_path = os.path.join(condor_folder, "run.sh")
    with open(executable_path, "w") as executable_file:
        run_sh = run_tmp.format(os.path.dirname(os.getcwd()), output_folder)
        executable_file.write(run_sh)

    # Create job config
    config_path = os.path.join(condor_folder, "job.config")
    with open(config_path, "w") as config_file:
        # Basic configs
        job_options = [
            "Executable = /bin/bash",
            "Universe = vanilla",
            # "Notification = Complete",
            "getenv = TRUE",
            "should_transfer_files = YES",
            "when_to_transfer_output = ON_EXIT",
            "\n",
        ]
        for line in job_options:
            config_file.write(line + "\n")
        # Job Queue
        for ky, val in sample_dict.items():
            sample_path = os.path.join(job_meta["input_folder"], val)
            config_file.write(
                "Arguments = {}/run.sh {} {}/{} \n".format(
                    condor_folder, sample_path, output_folder, ky
                )
            )
            config_file.write("Log       = {}/logs/{}.log\n".format(condor_folder, ky))
            config_file.write("Output    = {}/logs/{}.out\n".format(condor_folder, ky))
            config_file.write("Error     = {}/logs/{}.err\n".format(condor_folder, ky))
            config_file.write("Queue\n\n")
